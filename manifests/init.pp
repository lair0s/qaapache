class qaapache 

(
	$application_title = 'QA Web Site', #Default value
)

{
	if !$application_title {
		fail("Module ${module_name} is missing a parameter 'application title'")
	}

	$apachename = $::operatingsystem ? {
		centos => 'httpd',
		ubuntu => 'apache2',
		default => 'httpd',
	}

	package { $apachename:
		ensure => present,
	}

	file { '/var/www':
        	ensure => directory,
	}

	file { '/var/www/html':
		ensure => directory,
	}

	notify { 'ClientCertMessage':
		withpath => true,
		message => "The certificate name is ($clientcert)",
	}

	notify { 'DebugMessage':
		withpath => true,
		message => 'Generating index.html using erb template',
	}

	file { 'index.html':
        	ensure => file,
		path => '/var/www/html/index.html',
		content => template('qaapache/index.html.erb')
	}

	if $apachename == 'httpd' {
		$conffile = '/etc/httpd/conf/httpd.conf'

		file { '/etc/httpd/conf/httpd.conf':
			ensure => file,
			owner => 'root',
			group => 'root',
			source => 'puppet:///modules/qaapache/httpd.conf',
			require => Package[$apachename],
		}
	}

	service { $apachename:
		ensure => running,
		subscribe => File[$conffile],
	}
}
